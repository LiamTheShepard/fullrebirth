﻿using UnityEngine;
using System.Collections;



public class ClickPulpit : MonoBehaviour {

	private bool clicked = false;

	// Use this for initialization
	void Start () {	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnMouseDown() 
	{
		clicked = true;
	}

	void OnGUI()
	{
		if (clicked)
		{
			GUI.Box(new Rect(200,200,200,200), "");

			GUI.Label(new Rect(210,210, 150, 150), "The pulpit to give services from. You find nothing of interest.");

			if (GUI.Button(new Rect(210, 370, 80, 20), "Close")){
				clicked=false;
			};
		}
	}
}
