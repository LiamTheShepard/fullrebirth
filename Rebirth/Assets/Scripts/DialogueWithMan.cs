﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class DialogueWithMan : MonoBehaviour {

	//All declarations are at the top for neatness
	private bool playDia1 = false;
	private bool playDia2 = false;
	private bool playDia3 = false;
	private bool playedDia = false;
	private bool isClickable = true;
	private bool playLeave = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	//Detects when the object is left clicked by the user
	void OnMouseDown(){
		//Checks if the object is able to be interacted with.
		if (isClickable){
			//Checks if the initial dialogue has already been played
			if (playedDia == false) {
				playDia1 = true;
				isClickable = false;
			} else {
				playLeave = true;
			}
		}
	}

	void OnGUI() {
			if (playDia1) {

				GUI.Box(new Rect(200, 200, 300, 300), "");

				GUI.Label(new Rect(210, 210, 310, 310), "Ah father, hello. We were expecting you.");

				if (GUI.Button(new Rect(210 ,445, 280, 15), "Ah yes.Is there anything to report?")) {
						playDia2 = true;
						playDia1 = false;
				}

				if (GUI.Button(new Rect(210 ,470, 280, 15), "Of course you were, I told you to.")) {
					playDia3 = true;
					playDia1 = false;
				}

			}

			if (playDia2) {

				GUI.Box(new Rect(200, 200, 300, 300), "");

				GUI.Label(new Rect(210, 210, 310, 310), "No Father. Shall we continue?");

				if (GUI.Button(new Rect(210 ,445, 280, 15), "Of course. Let us continue onwards.")) {

				}
				
				if (GUI.Button(new Rect(210,470, 280, 15 ), "I require only a short moment more.")) {
					playDia2 = false;
					playDia1 = false;
					isClickable = true;
					playedDia = true;
				}
			}
			if (playDia3) {

				GUI.Box(new Rect(200, 200, 300, 300), "");

				GUI.Label(new Rect(210, 210, 310, 310), "Indeed, Father. Is there anything you want to know?");
				
			
			}
			if (playLeave){
				GUI.Box(new Rect(200, 200, 300, 300), "");
				
				GUI.Label(new Rect(210, 210, 310, 310), "Are you ready to depart father?");
				
				if (GUI.Button(new Rect(210 ,445, 280, 15), "Yes.")) {

				}

				if (GUI.Button(new Rect(210 ,470, 280, 15), "No. Just another moment.")) {
					playDia2 = false;
					playDia1 = false;
					playLeave = false;
					isClickable = true;
				}
			}
				 
	}

}