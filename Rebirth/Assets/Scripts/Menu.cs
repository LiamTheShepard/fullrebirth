﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	void OnGUI () {

		// Make a background box
		GUI.Box(new Rect(10,10,220,280), "Game menu");
		
		// Make the first button. This loads the first level
		if(GUI.Button(new Rect(20,40,200,100), "Start")) {
			Application.LoadLevel("Level 1");
		}
		
		// Make the second button. This quits the game. NOTE: This function only works when the games is compiled.
		if(GUI.Button(new Rect(20,170,200,100), "Quit")) {
			Application.Quit();
		}
	}
	
	// Update is called once per frame
	void Update () {

	}
}
